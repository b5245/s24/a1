2. 	// Find users who are from the HR department 
		// and their age is greater then or equal to 70.

db.users.find ({
	$and:[
		{ department: { $regex: 'HR', $options: '$i' } },
		{ age: { 
				$gte: 70
			}
	}
	]
}, 

{ 
	_id: 0
}).pretty();

    
3. // Find users with the letter 'e' in their 
	//first name and has an age of less than or equal to 30.

	db.users.find({
	$and: [
	{
		firstName: { $regex: 'e', $options: '$i' }
	},
	{age: {
		$lte: 30
	}
}]
})
